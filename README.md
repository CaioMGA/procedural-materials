# README #

List of Procedurally generated Unity materials.

### Downloads ###
* You can download the final versions at __Downloads__ folder
* Browse samples at __Samples__ folder
* Get the sources under __Source__ folder

### Contact ###

If you find this project useful or used the materials in your projects, I'd love to hear about it. Please e-mail me: [caiomga@gmail.com](mailto:caiomga@gmail.com)

### License ###

This work is licensed under a [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
